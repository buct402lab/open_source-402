# 402共用仓库使用教程
## git下载
* windows和ubuntu系统都需要下载git工具
* git官网https://www.git-scm.com/download/
## git使用
### ubuntu系统
 #### 首次使用git仓库
 * 首先，确认你的系统是否已安装git 可以通过 
       
        git    
     指令进行查看  
 * 如果没有，在命令行模式下输入  

        sudo apt-get install git    
    命令进行安装
* 安装好git后需将云端文件clone到本地进行修改   
            
        git clone https://gitee.com/buct-402-laboratory/buct402
#### 使用维护仓库代码
* 每次编辑之前在本地文件夹下拉取最新仓库（更新内容防止出错）

        git pull
* 更改文档代码后提交更改到暂存区

        git add .
* 编写本次提交版本名称

         git commit -m "你的版本名称"
* 更新本地文档到云端仓库（等待管理员确认）

        git push
### windows系统
 #### 首次使用git仓库
* 官网安装git

        https://www.git-scm.com/download/
* 打开git终端鼠标右键->git bash here 克隆仓库内容到本地

        git clone https://gitee.com/buct-402-laboratory/buct402

#### 使用维护仓库代码
* 同上述操作

## 注意事项！！！
* 每次使用前保证仓库最新否则会报错，更新内容写道文档中，按照标准格式添加，完成编辑后联系管理员

## .gitignore文件使用帮助（本地编辑中不需要上传的文件可以忽略）
* 使用规则  
待更新
     